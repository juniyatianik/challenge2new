package process;
import java.io.*;
import java.util.*;

import java.util.stream.Collectors;


public class ProcessClass implements ProcessInterface {

    protected static Long findLower(List<Integer> list, int target) {
        return list.stream()
                .filter(i -> i < target)
                .collect(Collectors.counting());
    }

    protected static Long findHigher(List<Integer> list, int target) {
        return list.stream()
                .filter(i -> i > target)
                .collect(Collectors.counting());
    }

    public Long groupHigher(String csvFile, int num) {

        try {
            File file = new File(csvFile);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = "";
            String[] tempArr;
            List<Integer> list = new ArrayList<>();
            Long result = 0L;
            while((line = bufferedReader.readLine()) != null) {
                tempArr = line.split(";");

                for(int i = 1; i < tempArr.length; ++i) {
                    list.add(Integer.parseInt(tempArr[i]));
                    result=findHigher(list, num);
                }
                System.out.println();
            }

            bufferedReader.close();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Long groupLower(String csvFile, int num) {

        try {
            File file = new File(csvFile);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = "";
            String[] tempArr;
            List<Integer> list = new ArrayList<>();
            Long result = 0L;
            while((line = bufferedReader.readLine()) != null) {
                tempArr = line.split(";");

                for(int i = 1; i < tempArr.length; ++i) {
                    list.add(Integer.parseInt(tempArr[i]));
                     result=findLower(list, num);
                }
                System.out.println();
            }

            bufferedReader.close();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public double generateMean(String csvFile) {
        try {
            File file = new File(csvFile);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = "";
            String[] tempArr;

            int total = 0;
            int total_length = 0;
            double mean = 0;
            while((line = bufferedReader.readLine()) != null) {

                tempArr = line.split(";");

                for(int i = 1; i < tempArr.length; ++i) {
                       total += Integer.parseInt(tempArr[i]);
                       total_length += 1;
                }

                System.out.println();
            }

            mean = total / total_length;

            bufferedReader.close();
            return mean;
        }catch(IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

//    method sort

   public int generateMedian(String csvFile) {
        try {
            File file = new File(csvFile);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = "";
            String[] tempArr;

            List<Integer> list = new ArrayList<>();
            int median = 0;
            while((line = bufferedReader.readLine()) != null) {

                tempArr = line.split(";");

                for(int i = 1; i < tempArr.length; ++i) {
                    list.add(Integer.parseInt(tempArr[i]));
                }

            }

            Collections.sort(list);
            int middle = list.size() / 2;
            if (list.size() % 2 == 0) {
                median = (list.get(middle) + list.get(middle - 1)) / 2;
            } else {
                median = list.get(middle);
            }

            bufferedReader.close();
            return median;
        }catch(IOException e) {
            e.printStackTrace();
        }
        return 0;
    }


    public int generateModus(String csvFile) {
        try {
            File file = new File(csvFile);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = "";
            List<String> tempArr;

            int maxCount = 0;
            int max = 0;
            while((line = bufferedReader.readLine()) != null) {
                tempArr = Arrays.asList(line.split(";"));

                for(int i = 1; i < tempArr.size(); ++i) {
                    int count = 0;
                    for(int j = 1; j < tempArr.size(); ++j) {
                        if(tempArr.get(i).equals(tempArr.get(j))) {
                            count += 1;
                        }
                    }
                    if(count > maxCount) {
                        maxCount = count;
                        max = Integer.parseInt(tempArr.get(i));
                    }
                }

                System.out.println();
            }

            bufferedReader.close();
            return max;
        }catch(IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

//    make method sorting list
    public static void sort(List<List<Integer>> list) {
//        for(int i = 0; i < list.size(); i++) {
//            for(int j = 0; j < list.size(); j++) {
//                if(list.get(i) > list.get(j)) {
//                    int temp = list.get(i);
//                    list.set(i, list.get(j));
//                    list.set(j, temp);
//                }
//            }
//        }

    }

}
