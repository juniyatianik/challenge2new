package process;

public interface ProcessInterface {
    public Long groupHigher(String csvFile, int num);
    public Long groupLower(String csvFile, int num);
    public double generateMean(String csvFile);
    public int generateMedian(String csvFile);
    public int generateModus(String csvFile);
}
